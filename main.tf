terraform {
  backend "http" {}
}

module "cluster" {
  source = "git::https://gitlab.com/k8s-plgrnd/tf-modules/hcloud-k3s-cluster.git?ref=master"

  hcloud_token    = var.hcloud_token
  worker_type     = "cx11"
  worker_count    = 2
  k8s_extra_flags = "--no-deploy traefik"
  node_locations  = ["hel1", "fsn1", "nbg1"]

  ssh_keys = {
    "zachu@Zophiel" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC49tjbDIHqhgV1eDUaXD9rOVbHEjvEto0fw0X67bj/HbtZh/2xqXCe3SHFmcUBrJ3FADqh81rsI1TL/cJR506GcLUMn9JyuKS7O3h2EITWvYJuc181/j8FibXpq16JB7UrwY44JJ4JYehvMxWXBAw/fxKJ8ddCXCDAuy9ZYIb9sW2cw3nmtLsMfJ26NuSscJUxILE51p2tEuHx2gljUtZla0fHFymnrV61wGqpqAEa0/xOnrE7pMgc86uVxGywdIjQDygBWkXilJqBA6dcU+6a3IjNw5kKr+1TymfHJigvt4OckDydydbu/I5MGIjStntM4yXarFydq92dDBQ8p/Da43rM9aCUyZsqg/HL8xMZ1iuzpWUGoQF90QtfQeZAlZOT1V7GDBNFfz9P8fBPJGj4Ld873N/MNI1ZzWzCRWiJ5Mg4hX/x7GeOnyP3Wf96Z7OZng6BaGn5MG4kSre+MgVcdY2l59Put3erXWORTsUEXnraH4JzQ7wUIlbIJYyf2cbvOAe5u83xGO05nFIwd2D7Z1JGcSJzXLON1EgE2euskiReddp7gl35JdYXmJM09OQ9GcrR2GFNlYLeO3InWxi6oXqiaghcD6WbbAnhsL/p5dmwfDxnqmN1CWWF6bcscttW3ovwF5RyGnDXSSZj0HewVUx8QDO85TdokuAa1vLh1w== zachu@Zophiel"
    "ci@gitlab"     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDur6Q9T5vjRiaPX+KPi9SltQT//rdQJJoCVafruvU9bGKG7fnS7muR71yYXiR06fh1TQOozoVDDKG2sl2IGV8u52HG6Vj50RlfNEcsPgiP637ljgX3XppvbE7H8s8JP/pyjDSj7EJN05Tl92CYErd9ikJ7Nz+4n3brpAzMRpfO/aYqbaijm8FukFi/SdLVDi9xHwZha0N/6AAWl9a0ClgUU93bcpVwcpLe5QeJrjydNeie759SmV6e1Tm6+nwx9ue3Zf+9QXzcSXcEs+GE58Jc4tldOocDu4R3ep7nCNIaD3wyWI7hhgWl7+O1NgBA+f7cKbQiSDW/qUYCAS9LUaF/+3D9nG0SJL6YHw2n0n1uFoJXlWx6oq9YM6z9eLi/RSZe9D4xeG+GrjdqbWk5eWaevy3fu9+pqbpvfDFHm3cystIWp6JFUfn8hRMa6q7Jau/H4G45y0BCuB6QITU2yRVNW3t/e5BFFyg8U8Xo4NwnMoh6vR8NxC1CirZ7y7v+nXcgln7XZFFF6JPFWRn1K04qTLi0WjkV7WXax9BLPHyrRJ9nTjiRbK66gDBOGRTKNHuR93dpVn1fKkf9M67dCQZ7megyRpy6QkK55hrOnsFahQTmgbfeVWrn++H0B3oMOWcBfuNx+ztmriJH3cKVvedeA12X95G1F/M30UtpbtRiYQ== ci@gitlab"
  }
}

resource "local_file" "kubeconfig" {
  sensitive_content = module.cluster.kubeconfig
  filename          = "kubeconfig"
  file_permission   = "0600"
}
