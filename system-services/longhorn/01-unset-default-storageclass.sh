#!/bin/sh
for sc in $(kubectl get storageclass -o name); do
  echo "Changing ${sc}"
  kubectl patch "${sc}" -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
done
