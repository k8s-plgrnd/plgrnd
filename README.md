Plgrnd
======
![pipeline](https://gitlab.com/k8s-plgrnd/plgrnd/badges/master/pipeline.svg)

In short this is a hobby-kubernetes-cluster using k3s. Modules separated to
[k8s-plgrnd/tf-modules](https://gitlab.com/k8s-plgrnd/tf-modules). This is supposed to be the entry-point and maybe the
management project.

TODO: Better README!

**Note**: Pipelines are disabled from guests because at the current stage they might reveal secrets.
